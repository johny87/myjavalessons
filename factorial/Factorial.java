//Factorial.java
//Created 04/15/2016 11:50 GMT+5
package factorial;
public class Factorial {
    public static long calculate(int x) {
        long f=1;
        for (int i=1;i<=x;i++) {
                f *= i;
        }
    return f;
    }
}
