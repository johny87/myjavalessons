//Triangle.java
//Created 04/01/16 12:10 GMT+5
package point;
public class Triangle {
    private Point p1,p2,p3;
    public Triangle (Point p1, Point p2, Point p3) {
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;
    }
    public double area () {
        if ( (p2.x - p1.x) * (p3.y -  p1.y) == (( p3.x - p1.x) * (p2.y - p1.y)) ) return Double.NaN;
        else {
            double A = Math.sqrt(Math.sqrt(Math.abs(p2.x-p1.x))+Math.sqrt(Math.abs(p2.y-p1.y)));
            double B = Math.sqrt(Math.sqrt(Math.abs(p3.x-p2.x))+Math.sqrt(Math.abs(p3.y-p2.y)));
            double C = Math.sqrt(Math.sqrt(Math.abs(p1.x-p3.x))+Math.sqrt(Math.abs(p1.y-p3.y)));
            double P = (A+B+C)/2;
            return Math.sqrt(P*(P-A)*(P-B)*(P-C));
            }
        }
}

/* Использована формула Герона :
module Герон1; 
    var X1,Y1,X2,Y2,X3,Y3,A,B,C,P : real; 
    procedure Длина (X,Y,Z,U : real):real; 
        begin return Sqrt(Sqr(Z-X) + Sqr(U-Y)) end Длина; 
begin 
    read(X1,Y1,X2,Y2,X3,Y3); 
    A := Длина (X1,Y1,X2,Y2); 
    B := Длина (X2,Y2,X3,Y3); 
    C := Длина (X3,Y3,X1,Y1); 
    P := (A+B+C)/2; 
    writeln ('Площадь равна',Sqrt(P$*$ (P-A)$*$ (P-B)$*$ (P-C))) 
end  Герон1. 
*/

