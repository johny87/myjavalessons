//InitPoint.java
//Created 03/30/16 14:55 GMT+5 
package point;
public class InitPoint {
    public static void main (String[] args) {
        Point point1 = new Point(5.0,5.0);
        Point point2 = new Point(5.5,5.5);
        Point point3 = new Point(6.0,10.0);
        //System.out.println(p1.getXY());
        Triangle triangle1 = new Triangle(point1, point2, point3); 
        System.out.println("S= " + triangle1.area());        
    }
}
