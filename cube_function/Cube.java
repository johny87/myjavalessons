//Cube.java
//Created 04/01/16 17:55 GMT+5

package cube_function;

public class Cube {
    private int a,b,c;
    public Cube (int a,int b, int c) {
        this.a=a;
        this.b=b;
        this.c=c;
    }
    public float calculate(int x) {
    return this.a*x*x+this.b*x+this.c;
    }
}
