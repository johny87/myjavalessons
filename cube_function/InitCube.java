//InitCube.java
//Created 04/01/16 17:55 GMT+5

package cube_function;
public class InitCube {
    public static void main (String[] args) {
        Cube cube1 = new Cube(Integer.parseInt(args[0]),Integer.parseInt(args[1]),Integer.parseInt(args[2]));
        if ( Integer.parseInt(args[3]) > Integer.parseInt(args[4])) {
            for (int i=Integer.parseInt(args[3]); i>Integer.parseInt(args[4]); i--) { 
                System.out.println(cube1.calculate(i));   
            }
        }
        else if ( Integer.parseInt(args[3]) < Integer.parseInt(args[4])) {
            for (int i=Integer.parseInt(args[3]); i<Integer.parseInt(args[4]); i++) {
                System.out.println(cube1.calculate(i));
            }
        } 
        else System.out.println(cube1.calculate(Integer.parseInt(args[4])));
    }
}
